# Створіть програму, яка складається з функції, яка приймає три числа і повертає їх середнє арифметичне,
# і головного циклу, що запитує у користувача числа і обчислює їх середні значення за допомогою створеної функції.


def average(a, b, c):
    return (a + b + c) / 3


while True:
    try:
        num1 = float(input("Enter the first number: "))
        num2 = float(input("Enter the second number: "))
        num3 = float(input("Enter the third number: "))

        result = average(num1, num2, num3)

        print("Average:", result)

        continue_counting = input("Do you want to continue? (Yes or No): ")
        if continue_counting != "Yes":
            break

    except ValueError:
        print("Invalid. Try again.")

# Створіть програму, яка приймає як формальні параметри зріст і вагу користувача, обчислює індекс маси тіла і
# в залежності від результату повертає інформаційне повідомлення (маса тіла в нормі,
# недостатня вага або слідкуйте за фігурою). Користувач з клавіатури вводить значення росту та маси тіла та
# передає ці дані у вигляді фактичних параметрів під час виклику функції.
# Програма працює доти, доки користувач не зупинить її комбінацією символів «off»


def bmi(weight, height):
    return round(weight / height**2, 1)


while True:
    try:
        weight = float(input("Enter your weight (kg): "))
        height = float(input("Enter your height (m): "))
        result = bmi(weight, height)
        print("BMI = ", result)
        if result <= 18.5:
            print("You are underweight. ")
        elif 18.5 <= result <= 24.9:
            print("Your body mass index is normal.")
        else:
            print(
                "You are overweight. Physical activities and a proper diet are highly recommended."
            )
        continue_counting = input("Do you want to continue? (yes/off) ")
        if continue_counting != "yes":
            break
    except ValueError:
        print("Invalid. Try again.")

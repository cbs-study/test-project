# Створіть програму-калькулятор, яка підтримує наступні операції: додавання, віднімання, множення, ділення,
# зведення в ступінь, зведення до квадратного та кубічного коренів. Всі дані повинні вводитися в циклі,
# доки користувач не вкаже, що хоче завершити виконання програми. Кожна операція має бути реалізована у вигляді
# окремої функції. Функція ділення повинна перевіряти дані на коректність та
# видавати повідомлення про помилку у разі спроби поділу на нуль.


def addition(a, b):
    return a + b


def subtraction(a, b):
    return a - b


def multiplication(a, b):
    return a * b


def division(a, b):
    if b == 0:
        ZeroDivisionError
    return a / b


def exponentiation(a, b):
    return a**b


def square_root(a):
    return a ** (1 / 2)


def cube_root(a):
    return a ** (1 / 3)


while True:
    try:
        print("Choose an operation:")
        print("1. Addition")
        print("2. Subtraction")
        print("3. Multiplication")
        print("4. Division")
        print("5. Exponentiation")
        print("6. Square Root")
        print("7. Cube Root")
        print("8. Exit")

        operation = int(input("Enter the number of the operation: "))

        if operation == 8:
            break
        a = float(input("Enter the first number: "))

        if operation in [1, 2, 3, 4, 5]:
            b = float(input("Enter the second number: "))

        if operation == 1:
            result = addition(a, b)
        elif operation == 2:
            result = subtraction(a, b)
        elif operation == 3:
            result = multiplication(a, b)
        elif operation == 4:
            result = division(a, b)
        elif operation == 5:
            result = exponentiation(a, b)
        elif operation == 6:
            result = square_root(a)
        elif operation == 7:
            result = cube_root(a)
        else:
            print("Invalid operation.")
            continue

        print("Result:", result)

    except ValueError as e:
        print("Error:", e)
    except Exception as e:
        print("An error occurred:", e)

# Створіть дві функції, що обчислюють значення певних алгебраїчних виразів.
# На екрані виведіть таблицю значень цих функцій від -5 до 5 з кроком 0.5.


def first_function(x):
    return 2 * x**2 - x + 1


def second_function(x):
    return x**2 - 3


table = [i * 0.5 for i in range(-10, 11)]
print("{:<10} {:<15} {:<15}".format("x", "First Function", "Second Function"))
for x in table:
    first_result = first_function(x)
    second_result = second_function(x)
    print("{:<10} {:<15} {:<15}".format(x, first_result, second_result))
